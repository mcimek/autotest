﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;

namespace AutoTest.WrapperFactory
{
    class DriverFactory
    {        
        private static IWebDriver driver;

        public static IWebDriver CreateDriver(String browserName)
        {
            switch (browserName)
            {
                case "Firefox":
                    driver = new FirefoxDriver();
                    break;
                case "Chrome":
                    driver = new ChromeDriver();
                    break;
            }
            
            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(60);
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);

            return driver;
        }


        public static void NavigateToUrl(String url)
        {
            driver.Url = url;
        }

        public static void CloseBrowser()
        {
            driver.Close();
        }
    }
}
