﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace AutoTest.PageObjects
{
    class HomePOM : Abstract.AbstractPOM
    {
        public HomePOM(IWebDriver driver) : base(driver)
        {
        }

        public override bool IsPageLoaded()
        {
            return Menu.IsPageLoaded();
        }
    }
}
