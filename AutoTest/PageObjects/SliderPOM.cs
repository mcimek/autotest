﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace AutoTest.PageObjects
{
    class SliderPOM : Abstract.AbstractPOM
    {
        [FindsBy(How = How.Id, Using = "amount1")]
        private IWebElement MinimumNumberOfBedrooms { get; set; }
        private IWebElement _sliderBar;

        [FindsBy(How = How.Id, Using = "slider-range-max")]
        private IWebElement SliderBar
        {
            get { return _sliderBar; }
            set
            {
                _sliderBar = value;
                SliderSelector = SliderBar.FindElement(By.TagName("span"));
            }
        }
        private IWebElement SliderSelector { get; set; }

        public SliderPOM(IWebDriver driver) : base(driver)
        {
        }

        public override bool IsPageLoaded()
        {
            List<IWebElement> listOfObjects = new List<IWebElement> { MinimumNumberOfBedrooms, SliderBar };
            return AllObjectsExist(listOfObjects) &&
                    Menu.IsPageLoaded();
        }


        public bool SetSlider(int valueToSet)
        {
            int valueToAdd = valueToSet - Int32.Parse(MinimumNumberOfBedrooms.GetProperty("value"));
            MoveSlider(SliderSelector, valueToAdd);

            return Int32.Parse(MinimumNumberOfBedrooms.GetProperty("value")) == valueToSet;
        }
    }
}
