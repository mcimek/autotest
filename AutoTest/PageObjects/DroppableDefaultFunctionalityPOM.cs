﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace AutoTest.PageObjects
{
    class DroppableDefaultFunctionalityPOM : Abstract.AbstractPOM
    {
        [FindsBy(How = How.Id, Using = "draggableview")]
        private IWebElement DragMeToMyTargetObject { get; set; }
        [FindsBy(How = How.Id, Using = "droppableview")]
        private IWebElement DropHereObject { get; set; }

        
        public DroppableDefaultFunctionalityPOM(IWebDriver driver) : base(driver)
        {
        }

        public override bool IsPageLoaded()
        {
            List<IWebElement> listOfObjects = new List<IWebElement> { DragMeToMyTargetObject, DropHereObject };
            return AllObjectsExist(listOfObjects) &&
                    Menu.IsPageLoaded();
        }


        public bool DragAndDrop()
        {
            DragAndDrop(DragMeToMyTargetObject, DropHereObject);
            
            return IsWithin();
        }

        public bool IsWithin()
        {
            int dragMeToMyTargetObjectX = DragMeToMyTargetObject.Location.X;
            int dragMeToMyTargetObjectY = DragMeToMyTargetObject.Location.Y;
            int dragMeToMyTargetObjectWidth = DropHereObject.Size.Width;
            int dragMeToMyTargetObjectHeight = DropHereObject.Size.Height;
            int dropHereObjectX = DropHereObject.Location.X;
            int dropHereObjectY = DropHereObject.Location.Y;
            int dropHereObjectWidth = DropHereObject.Size.Width;
            int dropHereObjectHeight = DropHereObject.Size.Height;

            return (dragMeToMyTargetObjectX + dragMeToMyTargetObjectWidth) > dropHereObjectX &&
                    dragMeToMyTargetObjectX < (dropHereObjectX + dropHereObjectWidth) &&
                    (dragMeToMyTargetObjectY + dragMeToMyTargetObjectHeight) > dropHereObjectY &&
                    dragMeToMyTargetObjectY < (dropHereObjectY + dropHereObjectHeight);
        }
        
        public bool IsDropped()
        {
            return DropHereObject.Text.Equals("Dropped!");
        }
    }
}
