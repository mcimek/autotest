﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace AutoTest.PageObjects
{
    class DemoQAMenuPOM
    {
        private IWebDriver driver;

        [FindsBy(How = How.LinkText, Using = "Registration")]
        private IWebElement RegistrationButton { get; set; }
        [FindsBy(How = How.LinkText, Using = "Droppable")]
        private IWebElement DroppableButton { get; set; }
        [FindsBy(How = How.LinkText, Using = "Datepicker")]
        private IWebElement DatepickerButton { get; set; }
        [FindsBy(How = How.LinkText, Using = "Slider")]
        private IWebElement SliderButton { get; set; }
        [FindsBy(How = How.Id, Using = "menu-item-98")]        
        private IWebElement TabsButton { get; set; }


        public DemoQAMenuPOM(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        public bool IsPageLoaded()
        {
            bool check;

            try
            {
                check = RegistrationButton.Enabled == true &&
                        DroppableButton.Enabled == true &&
                        DatepickerButton.Enabled == true &&
                        SliderButton.Enabled == true &&
                        TabsButton.Enabled == true;
            }
            catch (NoSuchElementException)
            {
                check = false;
            }

            return check;
        }


        public RegistrationPOM ClickRegistrationButton()
        {
            RegistrationButton.Click();
            return new RegistrationPOM(driver);
        }
        public DroppableDefaultFunctionalityPOM ClickDroppableButton()
        {
            DroppableButton.Click();
            return new DroppableDefaultFunctionalityPOM(driver);
        }
        public DatepickerDefaultFunctionalityPOM ClickDatepickerButton()
        {
            DatepickerButton.Click();
            return new DatepickerDefaultFunctionalityPOM(driver);
        }
        public SliderPOM ClickSliderButton()
        {
            SliderButton.Click();
            return new SliderPOM(driver);
        }
        public TabsPOM ClickTabsButton()
        {
            TabsButton.Click();
            return new TabsPOM(driver);
        }
    }
}
