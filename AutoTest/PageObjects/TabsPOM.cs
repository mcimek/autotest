﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace AutoTest.PageObjects
{
    class TabsPOM : Abstract.AbstractPOM
    {
        [FindsBy(How = How.CssSelector, Using = "ul[role='tablist']")]
        private IWebElement TabContainer { get; set; }


        public TabsPOM(IWebDriver driver) : base(driver)
        {
        }

        public override bool IsPageLoaded()
        {
            List<IWebElement> listOfObjects = new List<IWebElement> { TabContainer };
            return AllObjectsExist(listOfObjects) &&
                    Menu.IsPageLoaded();
        }

        
        public bool SelectTab(String tabName)
        {
            foreach (IWebElement tab in GetAllTabs())
            {
                if (tab.Text.Equals(tabName))
                {
                    tab.Click();
                }
            }

            return GetNameOfSelectedTab().Equals(tabName);
        }

        private IList<IWebElement> GetAllTabs()
        {
            return TabContainer.FindElements(By.TagName("li"));
        }

        public String GetNameOfSelectedTab()
        {
            String nameOfSelectedTab = null;

            foreach (IWebElement tab in GetAllTabs())
            {
                if (tab.GetAttribute("aria-selected").ToLower().Equals(true.ToString().ToLower()))
                {
                    nameOfSelectedTab = tab.Text;
                }
            }

            return nameOfSelectedTab;
        }
    }
}
