﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace AutoTest.PageObjects.Other
{
    class CalendarPOM
    {
        private IWebDriver driver;

        [FindsBy(How = How.ClassName, Using = "ui-datepicker-month")]
        private IWebElement CurrentMonth { get; set; }
        [FindsBy(How = How.ClassName, Using = "ui-datepicker-year")]
        private IWebElement CurrentYear { get; set; }
        [FindsBy(How = How.CssSelector, Using = "a.ui-datepicker-prev[data-handler='prev'][data-event='click']")]
        private IWebElement PreviousMonth { get; set; }
        [FindsBy(How = How.CssSelector, Using = "a.ui-datepicker-next[data-handler='next'][data-event='click']")]
        private IWebElement NextMonth { get; set; }
        [FindsBy(How = How.ClassName, Using = "ui-datepicker-calendar")]
        private IWebElement AllTheDaysContainer { get; set; }


        public CalendarPOM(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }


        public bool IsPageLoaded()
        {
            bool check;

            try
            {
                check = CurrentMonth.Enabled == true &&
                        CurrentYear.Enabled == true &&
                        PreviousMonth.Enabled == true &&
                        NextMonth.Enabled == true &&
                        AllTheDaysContainer.Enabled == true;
            }
            catch (NoSuchElementException)
            {
                check = false;
            }

            return check;
        }


        public void SetDate(DateTime date)
        {
            SetYear(date.Year);
            SetMonth(date.Month);
            ClickDay(date.Day);
        }

        public bool SetYear(int year)
        {
            int currentYearInt = Int32.Parse(CurrentYear.Text);
            int numberOfIterations = Math.Abs(currentYearInt - year);
            numberOfIterations = numberOfIterations * 12;

            for (int i = 0; i < numberOfIterations; i++)
            {
                if (currentYearInt == year)
                {
                    break;
                }
                else if (currentYearInt > year)
                {
                    PreviousMonth.Click();
                }
                else
                {
                    NextMonth.Click();
                }

                currentYearInt = Int32.Parse(CurrentYear.Text);
            }
            
            return Int32.Parse(CurrentYear.Text) == year;
        }
        public bool SetMonth(int month)
        {
            String expectedMonthName = new CultureInfo("en-US").DateTimeFormat.GetMonthName(month);

            String currentMonthName = CurrentMonth.Text;
            int currentMonthNumber = new CultureInfo("en-US").DateTimeFormat.MonthNames.ToList().IndexOf(currentMonthName) + 1;
            
            int numberOfIterations = Math.Abs(currentMonthNumber - month);

            for (int i = 0; i < numberOfIterations; i++)
            {
                if (currentMonthNumber > month)
                {
                    PreviousMonth.Click();
                }
                else
                {
                    NextMonth.Click();
                }

                currentMonthName = CurrentMonth.Text;
                currentMonthNumber = new CultureInfo("en-US").DateTimeFormat.MonthNames.ToList().IndexOf(currentMonthName) + 1;
            }

            return CurrentMonth.Text.Equals(expectedMonthName);
        }
        public bool ClickDay(int day)
        {
            IList<IWebElement> allDays = AllTheDaysContainer.FindElements(By.CssSelector("td[data-handler='selectDay']"));

            foreach (IWebElement dayObject in allDays)
            {
                if (Int32.Parse(dayObject.Text) == day)
                {
                    dayObject.Click();
                    break;
                }
            }

            return IsPageLoaded() == false;
        }
    }
}
