﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace AutoTest.PageObjects
{
    class RegistrationPOM : Abstract.AbstractPOM
    {
        [FindsBy(How = How.Id, Using = "name_3_firstname")]
        private IWebElement FirstNameTextField { get; set; }
        [FindsBy(How = How.Id, Using = "name_3_lastname")]
        private IWebElement LastNameTextField { get; set; }
        [FindsBy(How = How.Name, Using = "radio_4[]")]
        public IList<IWebElement> MaritalStatusRadioButtons { get; set; }
        [FindsBy(How = How.Name, Using = "checkbox_5[]")]
        public IList<IWebElement> HobbyCheckBoxes { get; set; }
        [FindsBy(How = How.Id, Using = "dropdown_7")]
        private IWebElement CountryDropdown { get; set; }
        [FindsBy(How = How.Id, Using = "mm_date_8")]
        private IWebElement MonthOfBirthDropdown { get; set; }
        [FindsBy(How = How.Id, Using = "dd_date_8")]
        private IWebElement DayOfBirthDropdown { get; set; }
        [FindsBy(How = How.Id, Using = "yy_date_8")]
        private IWebElement YearOfBirthDropdown { get; set; }
        [FindsBy(How = How.Id, Using = "phone_9")]
        private IWebElement PhoneNumberTextField { get; set; }
        [FindsBy(How = How.Id, Using = "username")]
        private IWebElement UsernameTextField { get; set; }
        [FindsBy(How = How.Id, Using = "email_1")]
        public IWebElement EMailTextField { get; set; }
        [FindsBy(How = How.Id, Using = "profile_pic_10")]
        private IWebElement SelectFileField { get; set; }
        [FindsBy(How = How.Id, Using = "description")]
        private IWebElement AboutYourselfTextArea { get; set; }
        [FindsBy(How = How.Id, Using = "password_2")]
        public IWebElement PasswordTextField { get; set; }
        [FindsBy(How = How.Id, Using = "confirm_password_password_2")]
        private IWebElement ConfirmPasswordTextField { get; set; }
        [FindsBy(How = How.Name, Using = "pie_submit")]
        private IWebElement SubmitButton { get; set; }

        [FindsBy(How = How.ClassName, Using = "piereg_message")]
        private IWebElement PositiveRegistrationConfirmation { get; set; }
        [FindsBy(How = How.ClassName, Using = "piereg_login_error")]
        private IWebElement NegativeRegistrationConfirmation { get; set; }


        public RegistrationPOM(IWebDriver driver) : base(driver)
        {
        }
        
        public override bool IsPageLoaded()
        {
            List<IWebElement> listOfObjects = new List<IWebElement> { FirstNameTextField, LastNameTextField, CountryDropdown,
                                                    MonthOfBirthDropdown, DayOfBirthDropdown, YearOfBirthDropdown, PhoneNumberTextField,
                                                    UsernameTextField, EMailTextField, SelectFileField, AboutYourselfTextArea,
                                                    PasswordTextField, ConfirmPasswordTextField, SubmitButton};
            return AllObjectsExist(listOfObjects) &&
                    MaritalStatusRadioButtons.Count > 0 &&
                    HobbyCheckBoxes.Count > 0 &&
                    Menu.IsPageLoaded();
        }


        public bool SetFirstName(String firstName)
        {
            return SetTextField(FirstNameTextField, firstName);
        }
        public bool SetLastName(String lastName)
        {
            return SetTextField(LastNameTextField, lastName);
        }
        public bool SetMaritialStatus(String maritialStatus)
        {
            return SetRadioButton(MaritalStatusRadioButtons, maritialStatus);
        }
        public bool SetHobby(String hobby)
        {
            return SetCheckBox(HobbyCheckBoxes, hobby);
        }
        public bool SetCountry(String countryName)
        {
            return SetDropdownField(CountryDropdown, countryName);
        }
        public bool SetDateOfBirth(int day, int month, int year)
        {
            return SetDayOfBirth(day) &&
                    SetMonthOfBirth(month) &&
                    SetYearOfBirth(year);
        }
        public bool SetDayOfBirth(int day)
        {
            return SetDropdownField(DayOfBirthDropdown, day);
        }
        public bool SetMonthOfBirth(int month)
        {
            return SetDropdownField(MonthOfBirthDropdown, month);
        }
        public bool SetYearOfBirth(int year)
        {
            return SetDropdownField(YearOfBirthDropdown, year);
        }
        public bool SetPhoneNumber(long phoneNumber)
        {
            return SetTextField(PhoneNumberTextField, phoneNumber);
        }
        public bool SetUsername(String username)
        {
            return SetTextField(UsernameTextField, username);
        }
        public bool SetEmail(String emailAdress)
        {
            return SetTextField(EMailTextField, emailAdress);
        }
        public bool SetProfilePicture(String picturePath)
        {
            return SetFile(SelectFileField, picturePath);
        }
        public bool SetAboutYourself(String description)
        {
            return SetTextField(AboutYourselfTextArea, description);
        }
        public bool SetPassword(String password)
        {
            return SetTextField(PasswordTextField, password);
        }
        public bool SetConfirmPassword(String password)
        {
            return SetTextField(ConfirmPasswordTextField, password);
        }
        public bool ClickSubmitButton()
        {
            SubmitButton.Click();
            return ObjectExists(PositiveRegistrationConfirmation);
        }
    }
}
