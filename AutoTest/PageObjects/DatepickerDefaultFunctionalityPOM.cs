﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace AutoTest.PageObjects
{
    class DatepickerDefaultFunctionalityPOM : Abstract.AbstractPOM
    {
        [FindsBy(How = How.Id, Using = "datepicker1")]
        private IWebElement DateTextField { get; set; }


        public DatepickerDefaultFunctionalityPOM(IWebDriver driver) : base(driver)
        {
        }

        public override bool IsPageLoaded()
        {
            List<IWebElement> listOfObjects = new List<IWebElement> { DateTextField };
            return AllObjectsExist(listOfObjects) &&
                    Menu.IsPageLoaded();
        }


        public bool SetDate(DateTime date)
        {
            SetDateUsingDatePicker(DateTextField, date);
            initObjects();

            return DateTextField.GetProperty("value").Equals(ConvertDateTime(date));
        }


        private String ConvertDateTime(DateTime date)
        {
            String month = new CultureInfo("en-US").DateTimeFormat.MonthNames.ToList()[date.Month - 1];

            return month + " " + date.Day + ", " + date.Year;
        }
    }
}
