﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;

using AutoTest.PageObjects.Other;

namespace AutoTest.PageObjects.Abstract
{
    abstract class AbstractPOM 
    {
        protected IWebDriver driver;
        private static DemoQAMenuPOM menuPrivate;

        public DemoQAMenuPOM Menu
        {
            get { return menuPrivate; }
            set { menuPrivate = value; }
        }

        public AbstractPOM(IWebDriver driver)
        {
            this.driver = driver;
            Menu = new DemoQAMenuPOM(driver);
            initObjects();
        }

        public abstract bool IsPageLoaded();

        protected bool SetTextField(IWebElement textField, String value)
        {
            textField.SendKeys(value);
            
            return textField.GetAttribute("value").Equals(value);
        }
        protected bool SetTextField(IWebElement textField, long value)
        {
            return SetTextField(textField, value.ToString());
        }

        protected bool SetDropdownField(IWebElement dropdownField, String value)
        {
            SelectElement dropdown = new SelectElement(dropdownField);
            dropdown.SelectByText(value);

            return dropdownField.GetAttribute("value").Equals(value);
        }
        protected bool SetDropdownField(IWebElement dropdownField, int value)
        {
            return SetDropdownField(dropdownField, value.ToString());
        }

        protected bool SetRadioButton(IList<IWebElement> listOfRadioButtons, String value)
        {
            bool check = false;

            foreach (IWebElement radioButton in listOfRadioButtons)
            {
                if (radioButton.GetAttribute("value").Trim().ToLower().Equals(value.Trim().ToLower()))
                {
                    radioButton.Click();
                    break;
                }
            }
            foreach (IWebElement radioButton in listOfRadioButtons)
            {
                if (radioButton.GetAttribute("value").Trim().ToLower().Equals(value.Trim().ToLower()))
                {
                    check = radioButton.Selected;
                }
            }

            return check;
        }

        protected bool SetCheckBox(IList<IWebElement> listOfCheckBoxes, String value)
        {
            bool check = false;

            foreach (IWebElement checkBox in listOfCheckBoxes)
            {
                if (checkBox.GetAttribute("value").Trim().ToLower().Equals(value.Trim().ToLower()))
                {
                    if (checkBox.Selected == false)
                    {
                        checkBox.Click();
                        break;
                    }
                }
            }
            
            foreach (IWebElement checkBox in listOfCheckBoxes)
            {
                if (checkBox.GetAttribute("value").Trim().ToLower().Equals(value.Trim().ToLower()))
                {
                        check = checkBox.Selected;
                }
            }
            
            return check;
        }

        protected bool SetFile(IWebElement chooseFileField, String filePath)
        {
            chooseFileField.SendKeys(filePath);

            String expectedFileName = Path.GetFileName(filePath);
            String actualFileName = Path.GetFileName(chooseFileField.GetAttribute("value"));

            return actualFileName.Equals(expectedFileName);
        }


        protected void DragAndDrop(IWebElement objectToDrag, IWebElement objectToDropOn)
        {
            Actions action = new Actions(driver);
            action.DragAndDrop(objectToDrag, objectToDropOn);
            action.Build();
            action.Perform();
        }

        protected void MoveSlider(IWebElement sliderObject, int valueToAdd)
        {
            String slidingDirection;
            if (valueToAdd < 0)
            {
                slidingDirection = Keys.ArrowLeft;
            }
            else
            {
                slidingDirection = Keys.ArrowRight;
            }

            Actions action = new Actions(driver);
            action.Click(sliderObject);
            action.Build();
            action.Perform();

            action = new Actions(driver);
            action.SendKeys(slidingDirection);
            action.Build();
            for (int i = 0; i < Math.Abs(valueToAdd); i++)
            {
                action.Perform();
            }
        }

        protected void SetDateUsingDatePicker(IWebElement objectWithDatePicker, DateTime date)
        {
            objectWithDatePicker.Click();
            
            CalendarPOM calendar = new CalendarPOM(driver);
            calendar.SetDate(date);
        }


        protected bool ObjectExists(IWebElement uiObject)
        {
            bool check;

            try
            {
                check = uiObject.Enabled == true;
            }
            catch (NoSuchElementException)
            {
                check = false;
            }

            return check;
        }
        protected bool AllObjectsExist(List<IWebElement> listOfObjects)
        {
            bool check = false;

            foreach (IWebElement uiObject in listOfObjects)
            {
                check = ObjectExists(uiObject);

                if (check == false)
                {
                    break;
                }
            }

            return check;
        }


        protected void initObjects()
        {
            PageFactory.InitElements(driver, this);
        }
    }
}
