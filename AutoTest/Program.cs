﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Reflection;

using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.PageObjects;
using NUnit.Framework;

using AutoTest.PageObjects;
using AutoTest.ExtensionMethods;
using AutoTest.WrapperFactory;

namespace AutoTest
{
    class Program
    {
        static void Main(string[] args)
        {
            IWebDriver driver;

            driver = DriverFactory.CreateDriver("Firefox");
            
            DriverFactory.NavigateToUrl("http://www.demoqa.com");

            HomePOM homePageObject = new HomePOM(driver);
            Assert.IsTrue(homePageObject.IsPageLoaded());

            DatepickerDefaultFunctionalityPOM datepickerPageObject = homePageObject.Menu.ClickDatepickerButton();
            Assert.IsTrue(datepickerPageObject.IsPageLoaded());

            DateTime dateToSet = DateTime.Today;
            Assert.IsTrue(datepickerPageObject.SetDate(dateToSet));

            dateToSet = dateToSet.AddMonths(4);
            Assert.IsTrue(datepickerPageObject.SetDate(dateToSet));

            dateToSet = dateToSet.AddYears(1);
            Assert.IsTrue(datepickerPageObject.SetDate(dateToSet));

            Assert.IsTrue(datepickerPageObject.SetDate(dateToSet));

            dateToSet = dateToSet.AddMonths(-2);
            Assert.IsTrue(datepickerPageObject.SetDate(dateToSet));

            dateToSet = dateToSet.AddYears(-1);
            Assert.IsTrue(datepickerPageObject.SetDate(dateToSet));

            DriverFactory.CloseBrowser();


            
            driver = DriverFactory.CreateDriver("Firefox");

            DriverFactory.NavigateToUrl("http://www.demoqa.com");

            homePageObject = new HomePOM(driver);
            Assert.IsTrue(homePageObject.IsPageLoaded());

            DroppableDefaultFunctionalityPOM droppablePageObject = homePageObject.Menu.ClickDroppableButton();
            Assert.IsTrue(droppablePageObject.IsPageLoaded());

            Assert.IsFalse(droppablePageObject.IsDropped());
            Assert.IsTrue(droppablePageObject.DragAndDrop());
            Assert.IsTrue(droppablePageObject.IsDropped());

            DriverFactory.CloseBrowser();
        }
    }
}
