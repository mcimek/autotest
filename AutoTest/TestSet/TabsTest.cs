﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Reflection;

using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.PageObjects;
using NUnit.Framework;

using AutoTest.PageObjects;

namespace AutoTest.TestSet
{
    class TabsTest : Abstract.AbstractTest
    {
        [Test]
        public void Tabs()
        {
            HomePOM homePageObject = new HomePOM(driver);
            Assert.IsTrue(homePageObject.IsPageLoaded());

            TabsPOM tabsPageObject = homePageObject.Menu.ClickTabsButton();
            Assert.IsTrue(tabsPageObject.IsPageLoaded());

            Assert.IsTrue(tabsPageObject.SelectTab("Tab 3"));
            Assert.IsTrue(tabsPageObject.SelectTab("Tab 2"));
            Assert.IsTrue(tabsPageObject.SelectTab("Tab 2"));
            Assert.AreEqual(tabsPageObject.GetNameOfSelectedTab(), "Tab 2");
        }
    }
}
