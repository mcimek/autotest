﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using NUnit.Framework;

using AutoTest.WrapperFactory;

namespace AutoTest.TestSet.Abstract
{
    abstract class AbstractTest
    {
        protected IWebDriver driver;

        [SetUp, Order(0)]
        public void OpenBrowser()
        {
            driver = DriverFactory.CreateDriver("Firefox");
        }

        [SetUp, Order(1)]
        public void NavigateToURL()
        {
            DriverFactory.NavigateToUrl("http://www.demoqa.com");
        }

        [TearDown]
        public void EndTest()
        {
            DriverFactory.CloseBrowser();
        }


        protected String GetProjectDirectory()
        {
            return Directory.GetParent(TestContext.CurrentContext.TestDirectory).Parent.FullName;
        }
    }
}
