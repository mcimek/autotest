﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Reflection;

using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.PageObjects;
using NUnit.Framework;

using AutoTest.PageObjects;

namespace AutoTest.TestSet
{
    class SliderTest : Abstract.AbstractTest
    {
        [Test]
        public void Slider()
        {
            HomePOM homePageObject = new HomePOM(driver);
            Assert.IsTrue(homePageObject.IsPageLoaded());

            SliderPOM sliderPageObject = homePageObject.Menu.ClickSliderButton();
            Assert.IsTrue(sliderPageObject.IsPageLoaded());

            Assert.IsTrue(sliderPageObject.SetSlider(5));
            Assert.IsTrue(sliderPageObject.SetSlider(3));
            Assert.IsTrue(sliderPageObject.SetSlider(3));
        }
    }
}
