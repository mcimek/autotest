﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Reflection;

using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.PageObjects;
using NUnit.Framework;

using AutoTest.PageObjects;

namespace AutoTest.TestSet
{
    class DatepickerTest : Abstract.AbstractTest
    {
        [Test]
        public void Datepicker()
        {
            HomePOM homePageObject = new HomePOM(driver);
            Assert.IsTrue(homePageObject.IsPageLoaded());

            DatepickerDefaultFunctionalityPOM datepickerPageObject = homePageObject.Menu.ClickDatepickerButton();
            Assert.IsTrue(datepickerPageObject.IsPageLoaded());

            DateTime dateToSet = DateTime.Today;
            Assert.IsTrue(datepickerPageObject.SetDate(dateToSet));

            dateToSet = dateToSet.AddMonths(4);
            Assert.IsTrue(datepickerPageObject.SetDate(dateToSet));

            dateToSet = dateToSet.AddYears(1);
            Assert.IsTrue(datepickerPageObject.SetDate(dateToSet));

            Assert.IsTrue(datepickerPageObject.SetDate(dateToSet));

            dateToSet = dateToSet.AddMonths(-2);
            Assert.IsTrue(datepickerPageObject.SetDate(dateToSet));

            dateToSet = dateToSet.AddYears(-1);
            Assert.IsTrue(datepickerPageObject.SetDate(dateToSet));
        }
    }
}
