﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Reflection;

using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.PageObjects;
using NUnit.Framework;

using AutoTest.PageObjects;
using AutoTest.ExtensionMethods;

namespace AutoTest.TestSet
{
    class RegistrationTest : Abstract.AbstractTest
    {
        [Test]
        public void Registration()
        {
            HomePOM homePageObject = new HomePOM(driver);
            Assert.IsTrue(homePageObject.IsPageLoaded());

            RegistrationPOM registrationPageObject = homePageObject.Menu.ClickRegistrationButton();
            Assert.IsTrue(registrationPageObject.IsPageLoaded());

            String username = "emozosia" + StringExtension.getRandomString(5);
            String pictureProfile = Path.Combine(GetProjectDirectory(), @"RegistrationProfilePicture\profilepicture.bmp");

            Assert.IsTrue(registrationPageObject.SetFirstName("Zosia"));
            Assert.IsTrue(registrationPageObject.SetLastName("Samosia"));
            Assert.IsTrue(registrationPageObject.SetMaritialStatus("Divorced"));
            Assert.IsTrue(registrationPageObject.SetMaritialStatus("Divorced"));
            Assert.IsTrue(registrationPageObject.SetMaritialStatus("Married"));
            Assert.IsTrue(registrationPageObject.SetHobby("Reading"));
            Assert.IsTrue(registrationPageObject.SetHobby("Cricket"));
            Assert.IsTrue(registrationPageObject.SetHobby("Cricket"));
            Assert.IsTrue(registrationPageObject.SetCountry("Italy"));
            Assert.IsTrue(registrationPageObject.SetDateOfBirth(11, 3, 2001));
            Assert.IsTrue(registrationPageObject.SetPhoneNumber(555666777999));
            Assert.IsTrue(registrationPageObject.SetUsername(username));
            Assert.IsTrue(registrationPageObject.SetEmail(username + "@email.pl"));
            Assert.IsTrue(registrationPageObject.SetProfilePicture(pictureProfile));
            Assert.IsTrue(registrationPageObject.SetAboutYourself("bla bla bla"));
            Assert.IsTrue(registrationPageObject.SetPassword("zlooo111222"));
            Assert.IsTrue(registrationPageObject.SetConfirmPassword("zlooo111222"));

            Assert.IsTrue(registrationPageObject.ClickSubmitButton());
        }
    }
}
