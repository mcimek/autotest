﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using System.Reflection;

using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.PageObjects;
using NUnit.Framework;

using AutoTest.PageObjects;

namespace AutoTest.TestSet
{
    class DroppableTest : Abstract.AbstractTest
    {
        [Test]
        public void Droppable()
        {
            HomePOM homePageObject = new HomePOM(driver);
            Assert.IsTrue(homePageObject.IsPageLoaded());

            DroppableDefaultFunctionalityPOM droppablePageObject = homePageObject.Menu.ClickDroppableButton();
            Assert.IsTrue(droppablePageObject.IsPageLoaded());

            Assert.IsFalse(droppablePageObject.IsDropped());
            Assert.IsTrue(droppablePageObject.DragAndDrop());
            Assert.IsTrue(droppablePageObject.IsDropped());
        }
    }
}
