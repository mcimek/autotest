﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoTest.ExtensionMethods
{
    public static class StringExtension
    {
        public static String getRandomString(int numberOfCharacters)
        {
            String characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            StringBuilder result = new StringBuilder(numberOfCharacters);

            Random random = new Random();

            for (int i = 0; i < numberOfCharacters; i++)
            {
                result.Append(characters[random.Next(characters.Length)]);
            }

            return result.ToString();
        }
    }
}
